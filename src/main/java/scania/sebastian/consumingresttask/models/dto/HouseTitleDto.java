package scania.sebastian.consumingresttask.models.dto;

import java.util.List;

public class HouseTitleDto {
    private List<String> titles;

    public HouseTitleDto(List<String> titles) {
        this.titles = titles;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }
}
