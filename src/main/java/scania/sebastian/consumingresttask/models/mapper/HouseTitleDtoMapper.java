package scania.sebastian.consumingresttask.models.mapper;

import org.json.JSONArray;
import scania.sebastian.consumingresttask.models.dto.HouseTitleDto;

import java.util.ArrayList;
import java.util.List;

public class HouseTitleDtoMapper {

    public HouseTitleDto mapHouseTitleDto(JSONArray jsonArray){
        List<String> titles = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++)
            titles.add(jsonArray.getString(i));

        return new HouseTitleDto(titles);
    }

}
