package scania.sebastian.consumingresttask.dataaccess;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import scania.sebastian.consumingresttask.models.dto.HouseTitleDto;
import scania.sebastian.consumingresttask.models.mapper.HouseTitleDtoMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.*;

@Component
public class HousesRepository {

    public HouseTitleDto getTitles() throws IOException {
        URL url = new URL("https://anapioficeandfire.com/api/houses/378");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            InputStreamReader reader = new InputStreamReader(connection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;
            StringBuffer response = new StringBuffer();

            while ((line = bufferedReader.readLine()) != null) {
                response.append(line);
            }
            bufferedReader.close();

            JSONObject jsonObject = new JSONObject(response.toString());
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONArray("titles"));
            HouseTitleDtoMapper houseTitleDtoMapper = new HouseTitleDtoMapper();

            return houseTitleDtoMapper.mapHouseTitleDto(jsonArray);
        }
        return null;
    }

}
