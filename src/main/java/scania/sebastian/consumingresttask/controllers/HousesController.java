package scania.sebastian.consumingresttask.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import scania.sebastian.consumingresttask.dataaccess.HousesRepository;
import scania.sebastian.consumingresttask.models.dto.HouseTitleDto;

import java.io.IOException;


@RestController
@RequestMapping("/api/v1/houses")
public class HousesController {
    @Autowired
    private HousesRepository housesRepository;

    @GetMapping
    public ResponseEntity<HouseTitleDto> getTitles() throws IOException {
        return new ResponseEntity<>(housesRepository.getTitles(), HttpStatus.OK);
    }
}
