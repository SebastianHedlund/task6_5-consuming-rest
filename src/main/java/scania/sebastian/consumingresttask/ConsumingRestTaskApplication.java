package scania.sebastian.consumingresttask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumingRestTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumingRestTaskApplication.class, args);
    }

}
